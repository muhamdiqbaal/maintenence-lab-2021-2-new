@extends('layouts.dashboard-layout')
@section('container')

 <!-- Main Wrapper -->
 <div class="main-wrapper">

    <!-- Loader -->
    <div id="loader-wrapper">
        <div id="loader">
            <div class="loader-ellips">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
        </div>
    </div>
    <!-- /Loader -->




    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Perbaikan Barang</h4>
                    <form class="forms-sample" action="/perbaikan-barang" method="POST">
                        @csrf
                      {{-- <div class="form-group">
                        <label for="exampleInputName1">Nama Barang</label>
                        <input type="text" name="barang_id" class="form-control" id="exampleInputName1" placeholder="Nama Barang">
                      </div> --}}
                      {{-- <div class="form-group">
                        <label for="exampleInputName1">Nama Barang</label>
                        <input type="number" name="barang_id" class="form-control" id="exampleInputName1" placeholder="Kerusakan Barang">
                      </div> --}}
                      <select class="form-select" name="barang_id">
                        @foreach ($barang as $item)
                        <option value="{{ $item->id}}">{{ $item->nama_barang }}</option>
                        @endforeach
                      </select>
                      <div class="form-group">
                        <label for="exampleInputName1">Kerusakan Barang</label>
                        <input type="text" name="kerusakan_barang" class="form-control" id="exampleInputName1" placeholder="Kerusakan Barang">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Tanggal Kerusakan Barang</label>
                        <input type="date" name="tgl_kerusakan" class="form-control" id="exampleInputName1">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Tanggal Perbaikan Barang</label>
                        <input type="date" name="tgl_perbaikan" class="form-control" id="exampleInputName1">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Status Barang</label>
                        <input type="text" name="status" class="form-control" id="exampleInputName1">
                      </div>
                      {{-- <div class="form-group">
                        <label for="exampleInputName1">Status</label>
                        <input type="date" name="tgl_perbaikan" class="form-control" id="exampleInputName1">
                      </div> --}}
                            <button type="submit" class="btn btn-info mr-2">Tambah <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <a href="/perbaikan-barang" class="btn btn-warning mr-2">Kembali <i class="mdi mdi-arrow-right btn-icon-prepend"></i></a>
                    </form>
                  </div>
                </div>

                </div>
              </div>
@endsection
