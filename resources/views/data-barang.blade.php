@extends('layouts.dashboard-layout')
@section('container')
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Loader -->
        <div id="loader-wrapper">
            <div id="loader">
                <div class="loader-ellips">
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                </div>
            </div>
        </div>
        <!-- /Loader -->




        <!-- Page Wrapper -->
        <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <h5 class="card-title">Tabel Data Barang</h5>
                    </div>
                </div>

                <a class="btn btn-info mb-4" href="/data-barang/create">Tambah Barang</a>
                <a class="btn btn-warning mb-4" href="/cetak-barang">Print</a>

                {{-- Table --}}

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Jumlah Barang</th>
                            <th scope="col">Merk Barang</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($barangs as $item)
                            <tr>
                                <th scope="row">{{ $item->nama_barang }}</th>
                                <td>{{ $item->jml_barang }}</td>
                                <td>{{ $item->merk_barang }}</td>
                                <td>
                                    <a type="button" href="/data-barang/{{$item->id}}/edit" class="btn btn-success mx-2"><i
                                            class="bi bi-pencil-square"></i></a>
                                    <form style="display: inline-block" action="/data-barang/{{ $item->id }}"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger"><i
                                                class="bi bi-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
