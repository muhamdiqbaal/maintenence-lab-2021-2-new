@extends('layouts.dashboard-layout')
@section('container')

 <!-- Main Wrapper -->
 <div class="main-wrapper">

    <!-- Loader -->
    <div id="loader-wrapper">
        <div id="loader">
            <div class="loader-ellips">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
        </div>
    </div>
    <!-- /Loader -->




    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit User</h4>
                    <form class="forms-sample" action="/data-user/{{$data_user->id}}" method="POST">
                        @csrf
                        @method('PUT')
                      <div class="form-group">
                        <label for="exampleInputName1">ID</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="ID Barang" value={{$data_user->id}} disable readonly>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Nama User</label>
                        <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="Nama User" value={{$data_user->name}}>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Email</label>
                        <input type="email" name="email" class="form-control" id="exampleInputName1" placeholder="Email" value={{$data_user->email}}>
                      </div>
                            <button type="submit" class="btn btn-primary mr-2">Ubah <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <a href="/data-user" class="btn btn-success mr-2">Kembali <i class="mdi mdi-arrow-right btn-icon-prepend"></i></a>

                    </form>

                </div>
              </div>
@endsection
