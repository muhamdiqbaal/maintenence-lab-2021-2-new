@component('mail::message')
# {{ $data['title'] }}

{{ $data['pesan'] }}
 
@component('mail::button', ['url' => $data['url']])
{{ $data['tombol'] }}
@endcomponent
 
Terimakasih, {{ $data['email'] }}<br>
{{ config('app.name') }}
@endcomponent