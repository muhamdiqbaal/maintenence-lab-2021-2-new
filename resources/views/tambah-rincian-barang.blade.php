@extends('layouts.dashboard-layout')
@section('container')
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Loader -->
        <div id="loader-wrapper">
            <div id="loader">
                <div class="loader-ellips">
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-elli@empty($record)

                @endemptyps__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                </div>
            </div>
        </div>
        <!-- /Loader -->




        <!-- Page Wrapper -->
        <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Tambah Rincian Barang</h4>
                            <form class="forms-sample" action="/rincian-barang" method="POST">
                                @csrf
                                <div class="form-group d-flex flex-column">
                                    <label>Nama Barang</label>
                                    <select class="form-select p-2" name="barang_id">
                                        @foreach ($barang as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName1">Tahun Pembuatan Barang</label>
                                    <input type="number" placeholder="YYYY" min="1999" max="2030"
                                        class="form-control" id="exampleInputName1" placeholder="Tahun Pembuatan Barang" name="thn_pembuatan_barang">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName1">Tanggal Masuk Barang</label>
                                    <input type="date" class="form-control" id="exampleInputName1" name="tgl_masuk_barang">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName1">Tanggal Keluar Barang</label>
                                    <input type="date" class="form-control" id="exampleInputName1" name="tgl_keluar_barang">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputName1">Harga Barang</label>
                                    <input type="number" class="form-control" id="exampleInputName1" name="harga_barang">
                                </div>
                                <button type="submit" class="btn btn-info mr-2">Tambah <i
                                        class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                                <a href="/rincian-barang" class="btn btn-warning mr-2">Kembali <i
                                        class="mdi mdi-arrow-right btn-icon-prepend"></i></a>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        @endsection
