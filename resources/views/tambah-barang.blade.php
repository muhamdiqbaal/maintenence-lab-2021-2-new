@extends('layouts.dashboard-layout')
@section('container')

 <!-- Main Wrapper -->
 <div class="main-wrapper">

    <!-- Loader -->
    <div id="loader-wrapper">
        <div id="loader">
            <div class="loader-ellips">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
        </div>
    </div>
    <!-- /Loader -->




    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Tambah Barang</h4>
                    <form class="forms-sample" action="/data-barang" method="POST">
                        @csrf
                      {{-- <div class="form-group">
                        <label for="exampleInputName1">ID Barang</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="ID Barang">
                      </div> --}}
                      <div class="form-group">
                        <label for="exampleInputName1">Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" id="exampleInputName1" placeholder="Nama Barang">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Jumlah Barang</label>
                        <input type="number" name="jml_barang" class="form-control" id="exampleInputName1" placeholder="Jumlah Barang">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Merk Barang</label>
                        <input type="text" name="merk_barang" class="form-control" id="exampleInputName1" placeholder="Merk">
                      </div>
                            <button type="submit" class="btn btn-info mr-2">Tambah <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <a href="/data-barang" class="btn btn-warning mr-2">Kembali <i class="mdi mdi-arrow-right btn-icon-prepend"></i></a>

                    </form>

                </div>
              </div>
@endsection
