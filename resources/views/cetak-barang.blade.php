<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan Barang</title>
    <style>
        .center {
            margin: auto;
        }
    </style>
</head>

<body>
    <div class="container" style="display: flex; flex-direction: column;">
        <div class="form-field">
            <h3 style="text-align: center"><b>Laporan Barang</b></h3>
        </div>
        <div class="table center" style="text-align: center">
            <table border="1px">
                <thead>
                    <tr>
                        <th scope="col">ID Barang</th>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Jumlah Barang</th>
                        <th scope="col">Merk Barang</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($barang as $item)
                        <tr>
                            <th>{{ $item->id}}</th>
                            <th>{{ $item->nama_barang }}</th>
                            <td>{{ $item->jml_barang }}</td>
                            <td>{{ $item->merk_barang }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>


</html>
