<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                        <li><a href="/">Dashboard</a></li>
                        @if (Auth::user()->level == 'admin')
                        <li><a href="/data-user">User</a></li>
                        @endif
                        <li><a href="/data-barang">Data Barang</a></li>
                        <li><a href="/perbaikan-barang">Perbaikan Barang</a></li>
                        <li><a href="/rincian-barang">Rincian Barang</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
