@extends('layouts.dashboard-layout')
@section('container')
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Loader -->
        <div id="loader-wrapper">
            <div id="loader">
                <div class="loader-ellips">
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                </div>
            </div>
        </div>
        <!-- /Loader -->




        <!-- Page Wrapper -->
        <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <h5 class="card-title">Tabel Rincian Barang</h5>
                    </div>
                </div>

                <a class="btn btn-info mb-4" href="/rincian-barang/create">Tambah Rincian Barang</a>
                <a class="btn btn-warning mb-4" href="/cetak-rincian-barang">Print</a>


                {{-- Table --}}

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Merk Barang</th>
                            <th scope="col">Tahun Pembuatan Barang</th>
                            <th scope="col">Tanggal Masuk Barang</th>
                            <th scope="col">Tanggal Keluar Barang</th>
                            <th scope="col">Harga Barang</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rincian_barangs as $item)
                        <tr>
                            <th>{{$item->barang->nama_barang}}</th>
                            <th>{{$item->barang->merk_barang}}</th>
                            <th>{{$item->thn_pembuatan_barang}}</th>
                            <td>{{$item->tgl_masuk_barang->translatedFormat('Y-m-d')}}</td>
                            <td>{{$item->tgl_keluar_barang->translatedFormat('Y-m-d')}}</td>
                            <td>{{$item->harga_barang}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
