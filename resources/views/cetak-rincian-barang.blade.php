<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan Barang</title>
    <style>
        .center {
            margin: auto;
        }
    </style>
</head>

<body>
    <div class="container" style="display: flex; flex-direction: column;">
        <div class="form-field">
            <h3 style="text-align: center"><b>Laporan Barang</b></h3>
        </div>
        <div class="table center" style="text-align: center">
            <table class="table" border="1px">
                <thead>
                    <tr>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Merk Barang</th>
                        <th scope="col">Tahun Pembuatan Barang</th>
                        <th scope="col">Tanggal Masuk Barang</th>
                        <th scope="col">Tanggal Keluar Barang</th>
                        <th scope="col">Harga Barang</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rincian_barangs as $item)
                    <tr>
                        <th>{{$item->barang->nama_barang}}</th>
                        <th>{{$item->barang->merk_barang}}</th>
                        <th>{{$item->thn_pembuatan_barang}}</th>
                        <td>{{$item->tgl_masuk_barang->translatedFormat('Y-m-d')}}</td>
                        <td>{{$item->tgl_keluar_barang->translatedFormat('Y-m-d')}}</td>
                        <td>{{$item->harga_barang}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>


</html>
