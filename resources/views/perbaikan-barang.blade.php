@extends('layouts.dashboard-layout')
@section('container')
    <!-- Main Wrapper -->
    <div class="main-wrapper">

        <!-- Loader -->
        <div id="loader-wrapper">
            <div id="loader">
                <div class="loader-ellips">
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                    <span class="loader-ellips__dot"></span>
                </div>
            </div>
        </div>
        <!-- /Loader -->




        <!-- Page Wrapper -->
        <div class="page-wrapper">

            <!-- Page Content -->
            <div class="content container-fluid">

                <div class="card" style="width: 100%;">
                    <div class="card-body">
                        <h5 class="card-title">Tabel Perbaikan Barang</h5>
                    </div>
                </div>

                <a class="btn btn-info mb-4" href="/perbaikan-barang/create">Tambah Perbaikan Barang</a>
                <a class="btn btn-warning mb-4" href="/cetak-perbaikan-barang">Print</a>

                {{-- Table --}}

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Kerusakan Barang</th>
                            <th scope="col">Tanggal Kerusakan Barang</th>
                            <th scope="col">Tanggal Perbaikan Barang</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kerusakanBarangs as $item)
                        <tr>
                            <th scope="row">{{ $item->barang->nama_barang }}</th>
                            <th scope="row">{{ $item->kerusakan_barang }}</th>
                            <td>{{ $item->tgl_kerusakan->translatedFormat('Y-m-d') }}</td>
                            <td>{{ $item->tgl_perbaikan->translatedFormat('Y-m-d') }}</td>
                            <th scope="row">{{ $item->status }}</th>
                            <td>
                                <a type="button" class="btn btn-success mx-2" href="/perbaikan-barang/{{$item->id}}/edit"><i
                                        class="bi bi-pencil-square"></i></a>
                                        <form style="display: inline-block" action="/perbaikan-barang/{{ $item->id }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger"><i
                                                    class="bi bi-trash"></i></button>
                                        </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /Page Content -->

        </div>
        <!-- /Page Wrapper -->

    </div>
    <!-- /Main Wrapper -->
@endsection
