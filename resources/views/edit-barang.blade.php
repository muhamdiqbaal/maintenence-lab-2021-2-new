@extends('layouts.dashboard-layout')
@section('container')

 <!-- Main Wrapper -->
 <div class="main-wrapper">

    <!-- Loader -->
    <div id="loader-wrapper">
        <div id="loader">
            <div class="loader-ellips">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
        </div>
    </div>
    <!-- /Loader -->




    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Barang</h4>
                    <form class="forms-sample" action="/data-barang/{{$barangs->id}}" method="POST">
                        @csrf
                        @method('PUT')
                      <div class="form-group">
                        <label for="exampleInputName1">ID Barang</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="ID Barang" value={{$barangs->id}} disable readonly>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" id="exampleInputName1" placeholder="Nama Barang" value={{$barangs->nama_barang}}>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Jumlah Barang</label>
                        <input type="number" name="jml_barang" class="form-control" id="exampleInputName1" placeholder="Jumlah Barang" value={{$barangs->jml_barang}}>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Merk Barang</label>
                        <input type="text" name="merk_barang" class="form-control" id="exampleInputName1" placeholder="Merk" value={{$barangs->merk_barang}}>
                      </div>
                            <button type="submit" class="btn btn-primary mr-2">Ubah <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <a href="/data-barang" class="btn btn-success mr-2">Kembali <i class="mdi mdi-arrow-right btn-icon-prepend"></i></a>

                    </form>

                </div>
              </div>
@endsection
