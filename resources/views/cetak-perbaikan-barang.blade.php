<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan Barang</title>
    <style>
        .center {
            margin: auto;
        }
    </style>
</head>

<body>
    <div class="container" style="display: flex; flex-direction: column;">
        <div class="card">
            <h3 style="text-align: center"><b>Laporan Perbaikan Barang</b></h3>
        </div>
        <div class="table center" style="text-align: center">
            <table border="1px" class="table">
                <thead>
                    <tr>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Kerusakan Barang</th>
                        <th scope="col">Tanggal Kerusakan Barang</th>
                        <th scope="col">Tanggal Perbaikan Barang</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($perbaikanBarang as $item)
                    <tr>
                        <th scope="row">{{ $item->barang->nama_barang }}</th>
                        <th scope="row">{{ $item->kerusakan_barang }}</th>
                        <td>{{ $item->tgl_kerusakan->translatedFormat('Y-m-d') }}</td>
                        <td>{{ $item->tgl_perbaikan->translatedFormat('Y-m-d') }}</td>
                        <th scope="row">{{ $item->status }}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>


</html>
