@extends('layouts.dashboard-layout')
@section('container')

 <!-- Main Wrapper -->
 <div class="main-wrapper">

    <!-- Loader -->
    <div id="loader-wrapper">
        <div id="loader">
            <div class="loader-ellips">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
        </div>
    </div>
    <!-- /Loader -->




    <!-- Page Wrapper -->
    <div class="page-wrapper">

        <!-- Page Content -->
        <div class="content container-fluid">

            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Status Perbaikan Barang</h4>
                    <form class="forms-sample" action="/perbaikan-barang/{{$perbaikan->id}}" method="POST">
                        @csrf
                        @method('PUT')
                      <div class="form-group">
                        <label for="exampleInputName1">Status Barang</label>
                        <input type="text" name="status" class="form-control" id="exampleInputName1" value="{{$perbaikan->status}}">
                      </div>
                      {{-- <div class="form-group">
                        <label for="exampleInputName1">Status</label>
                        <input type="date" name="tgl_perbaikan" class="form-control" id="exampleInputName1">
                      </div> --}}
                            <button type="submit" class="btn btn-primary mr-2">Edit <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <a href="/perbaikan-barang" class="btn btn-success mr-2">Kembali <i class="mdi mdi-arrow-right btn-icon-prepend"></i></a>
                    </form>
                  </div>
                </div>

                </div>
              </div>
@endsection
