@extends('layouts.main')
@section('container')
    <section class="ftco-section">
        <section class="vh-100">
            <div class="container py-5 h-100">
                <div class="row d-flex align-items-center justify-content-center h-100">
                    <div class="col-md-8 col-lg-7 col-xl-6">
                        <img src="{{asset('assets/img/smkmvp.png')}}"
                            class="img-fluid" alt="Phone image">
                    </div>
                    <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                        <div class="mb-4">
                            <h3>Forgot your password?</h3>
                            <p class="text-secondary">No problem.Just let us know your email adress and we will email you a password reset link that will allow you to choose a new one.</p>
                        </div>
                        <form action="/login" method="POST">
                            @csrf
                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" id="form1Example13" class="form-control form-control-lg" name="email" />
                                <label class="form-label" for="form1Example13">Email address</label>
                            </div>

                            <div class="d-flex justify-content-around align-items-center mb-4">


                            <!-- Submit button -->
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Email Password Reset Link</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    @endsection
