@extends('layouts.main')
@section('container')
    <section class="ftco-section">
        <section class="vh-100">
            @if(session()->has('loginError'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ session('loginError') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            <div class="container py-5 h-100">
                <div class="row d-flex align-items-center justify-content-center h-100">
                    <div class="col-md-8 col-lg-7 col-xl-6">
                        <img src="{{asset('assets/img/smkmvp.png')}}"
                            class="img-fluid" alt="Phone image">
                    </div>

                    <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                        <div class="mb-4">
                            <h3>Welcome to Maintenance Lab SMK MVP ARS Internasional</h3>
                            <p class="text-secondary">Please login first</p>
                        </div>
                        <form action="/login" method="POST">
                            @csrf
                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" id="form1Example13" class="form-control form-control-lg" name="email" placeholder="Email Address" />
                                <label class="form-label" for="form1Example13">Email Address</label>
                            </div>

                            <!-- Password input -->
                            <div class="form-outline mb-4">
                                <input type="password" id="form1Example23" class="form-control form-control-lg" name="password" placeholder="Password" />
                                <label class="form-label" for="form1Example23">Password</label>
                                <a href="/auth/forget-pass" class="auth-link text-black" style="float: right;">Forgot password?</a>
                            </div>

                            <div class="d-flex justify-content-around align-items-center mb-4">


                            <!-- Submit button -->
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                            <a type="button" href="/register" class="btn btn-warning btn-lg btn-block">Register</a>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    @endsection
