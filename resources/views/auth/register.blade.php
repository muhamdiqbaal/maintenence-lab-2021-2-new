@extends('layouts.main')
@section('container')
    <section class="ftco-section">
        <section class="vh-100">

            <div class="container py-5 h-100">
                <div class="row d-flex align-items-center justify-content-center h-100">
                    <div class="col-md-8 col-lg-7 col-xl-6">
                        <img src="{{asset('assets/img/smkmvp.png')}}"
                            class="img-fluid" alt="Phone image">
                    </div>
                    <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                        <div class="mb-4">
                            <h2>Register Here</h2>
                        </div>
                        <form action="/register" method="POST">
                            @csrf
                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="text" id="form1Example13" class="form-control form-control-lg" name="name" />
                                <label class="form-label" for="form1Example13">Name</label>
                            </div>

                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" id="form1Example13" class="form-control form-control-lg" name="email" />
                                <label class="form-label" for="form1Example13">Email address</label>
                            </div>

                            <!-- Password input -->
                            <div class="form-outline mb-4">
                                <input type="password" id="form1Example23" class="form-control form-control-lg" name="password" />
                                <label class="form-label" for="form1Example23">Password</label>
                            </div>

                            <div class="d-flex justify-content-around align-items-center mb-4">

                            <!-- Submit button -->
                            <a type="button" href="/login" class="btn btn-warning btn-lg btn-block">Back</a>
                            <button type="submit" class="btn btn-primary btn-lg btn-block mx-2">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    @endsection
