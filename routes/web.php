<?php

use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\PerbaikanBarangController;
use App\Http\Controllers\ForgotpasswordController;
use App\Http\Controllers\CetakBarangController;
use App\Http\Controllers\RincianBarangController;
use App\Http\Controllers\UserController;
use App\Htpp\Middleware\CekLevel;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

// Auth::routes(['verify' => true]);
Route::resource('/data-barang', BarangController::class);
Route::resource('/data-user', UserController::class)->middleware('auth','ceklevel:admin');
Route::resource('/perbaikan-barang', PerbaikanBarangController::class);
Route::resource('/rincian-barang', RincianBarangController::class);


// Route::get('/rincian-barang', function () {
//     return view('rincian-barang');
// });

// Route::get('/tambah-rincian-barang', function () {
//     return view('tambah-rincian-barang');
// });

Route::get('/holiday', function () {
    return view('holiday');
});

Route::get('/cetak-barang', [CetakBarangController::class, 'index']);
Route::get('/cetak-perbaikan-barang', [CetakBarangController::class, 'cetakPerbaikan']);
Route::get('/cetak-rincian-barang', [CetakBarangController::class, 'cetakRincian']);
Route::get('/auth/forget-pass', [ForgotPasswordController::class,'forget']);
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login',[LoginController::class, 'authenticate']);
Route::post('/logout',[LoginController::class, 'logout']);
Route::resource('/register', RegisterController::class);
