@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Barang</h4>
                    <form class="forms-sample">
                      <div class="form-group">
                        <label for="exampleInputName1">ID Barang</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="ID Barang">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Nama Barang</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama Barang">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Status Barang</label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="Status">
                      </div>
                            <button type="submit" class="btn btn-primary mr-2">Add <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <!-- <button type="submit" class="btn btn-danger">Cancel <i class="mdi mdi-close btn-icon-prepend"></i></button> -->
                    </form>
                  </div>
                </div>
              </div>
@endsection