<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRincianBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rincian_barangs', function (Blueprint $table) {
            $table->id();
            $table->foreignid('barang_id');
            $table->integer('thn_pembuatan_barang');
            $table->dateTime('tgl_masuk_barang');
            $table->dateTime('tgl_keluar_barang');
            $table->integer('harga_barang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rincian_barangs');
    }
}
