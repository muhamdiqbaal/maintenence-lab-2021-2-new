<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerbaikanBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perbaikan_barangs', function (Blueprint $table) {
            $table->id();
            $table->foreignid('barang_id');
            $table->string('kerusakan_barang');
            $table->dateTime('tgl_kerusakan');
            $table->dateTime('tgl_perbaikan');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perbaikan_barangs');
    }
}
