<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\perbaikan_barang;
use App\Models\barang;


class PerbaikanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(barang $barangs)
    {
        $kerusakanBarangs = perbaikan_barang::all();
        return view('perbaikan-barang', [
            'barang' => $barangs->nama_barang,
            'kerusakanBarangs' => $kerusakanBarangs
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(barang $barangs)
    {
        return view('tambah-perbaikan-barang', [
            'barang' => barang::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kerusakan_barang' => 'required',
            'barang_id' => 'required',
            'tgl_kerusakan' => 'required',
            'tgl_perbaikan' => 'required',
            'status' => 'required',
        ]);

        perbaikan_barang::create($validatedData);
        return redirect('/perbaikan-barang')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perbaikan = perbaikan_barang::findOrFail($id);
        return view('edit-perbaikan-barang', compact('perbaikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $input = $request->all();
        $perbaikan_barang = perbaikan_barang::find($id);
        $perbaikan_barang->update($input);
        return redirect('perbaikan-barang')->with('success', 'Status Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
