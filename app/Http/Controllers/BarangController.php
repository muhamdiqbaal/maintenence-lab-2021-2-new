<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\barang;
use App\Models\perbaikan_barang;
use App\Models\rincian_barang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Session;



class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = DB::table('barangs')->get();
        return view('data-barang', [
            'barangs' => $barang
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah-barang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_barang' => 'required',
            'jml_barang' => 'required',
            'merk_barang' => 'required',
        ]);

        barang::create($validatedData);
        return redirect('/data-barang')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barangs = barang::findOrFail($id);
        return view('edit-barang', compact('barangs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_barang' => 'required',
            'jml_barang' => 'required',
            'merk_barang' => 'required',
        ]);

        $input = $request->all();
        $barang = barang::find($id);
        $barang->update($input);
        return redirect('data-barang')->with('success', 'Data Barang Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(barang $barang, $id, perbaikan_barang $perbaikan_barang, rincian_barang $rincian_barang)
    {

        $barang = barang::find($id);
        $barang->delete();
        $barang->rincian_barang()->delete();
        $barang->perbaikan_barang()->delete();

        barang::whereId($id)->update(['id' => null]);

        return redirect('/data-barang')->with('success', 'barang berhasil dihapus');
    }
}
