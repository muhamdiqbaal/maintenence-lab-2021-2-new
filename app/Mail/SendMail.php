<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $subject)
    {
        $this->data = $data;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emailku')
                    ->from('muhammadiqbalftr@gmail.com', 'Maintenance Lab MVP')
                    ->subject($this->subject)
                    ->with('data', $this->data);
                    // ->attach(public_path('/templates/screenshot.jpg'), [
                    //     'as'    => 'screenshot.jpg',
                    //     'mime'  => 'image/jpg',
                    // ]);
    }
}
