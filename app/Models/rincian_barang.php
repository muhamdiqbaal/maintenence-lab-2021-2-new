<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\barang;

class rincian_barang extends Model
{
    use HasFactory;

    protected $dates = ['tgl_masuk_barang', 'tgl_keluar_barang'];
    protected $guarded = ['id'];

    public function barang()
    {
        return $this->belongsTo(barang::class);
    }
}
