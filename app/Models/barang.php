<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\perbaikan_barang;
use App\Models\rincian_barang;


class barang extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function perbaikan_barang()
    {
        return $this->hasMany(perbaikan_barang::class);

    }

    public function rincian_barang()
    {
        return $this->hasMany(rincian_barang::class);
    }
}
