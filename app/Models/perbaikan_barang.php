<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\barang;


class perbaikan_barang extends Model
{
    use HasFactory;

    protected $dates = ['tgl_kerusakan', 'tgl_perbaikan'];
    protected $guarded = ['id'];

    public function barang()
    {
        return $this->belongsTo(barang::class);
    }
}
